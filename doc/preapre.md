# Подготовка перед запуском

## Gogs
Конфигурационный файл находится в `config/gogs_conf/app.ini`
Рекомендуется менять только `EXTERNAL_URL` - адрес, где будет располагаться приложение.

## Strider
Нужно добавить свой ip в MongoDB или создать свою базу данных и определить ее в Dockerfile в переменной `DB_URI`
Как добавить свой адрес:
1. Перейти по [ссылке](https://account.mongodb.com/account/login)
2. Email Address: daynfgfgg@gmail.com  Pаsswоrd: unix-final
3. Нажать слава в меню-списке на Network Access
4. Нажать с права на зеленую кнопку ADD IP ADDRESSю
5. Нажать на ADD CURRENT IP ADDRESS в выпавшем меню, кнопка по центру

## Taiga
### Backend
Конфигурационные файлы находятся в `config/taiga_back_conf`.
Рекомендуется менять только файлы `local.py` cекцию `sites`.

### Frontend
Нужно добавить адрес backend-приложения в `config/taiga_front_conf/taiga_conf/conf.json` в строке `api`.
Вы можете изменить проксирование в `config/taiga_front_conf/nginx_config/taiga.conf` (НЕ РЕКОМЕНДУЕТСЯ).
