# Описание Dockerfile для сервиса gogs

Первая ступень контейнера основывается на `golang:alpine3.11`
```
FROM golang:alpine3.11 AS binarybuilder
```
Установка зависимостей.
```
RUN apk --no-cache --no-progress add --virtual \
  build-deps \
  build-base \
  git \
  linux-pam-dev
```
Задание рабочего каталога.
```
WORKDIR /gogs.io/gogs
```
Скачка и компилирование `gogs`.
```
RUN git clone https://github.com/gogs/gogs .
RUN make build-no-gen TAGS="cert pam"
```
Вторая ступень контейнера основывается на `alpine:3.11`
```
FROM alpine:3.11
```
Установка зависимостей.
```
ADD https://github.com/tianon/gosu/releases/download/1.11/gosu-amd64 /usr/sbin/gosu
RUN chmod +x /usr/sbin/gosu \
  && echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories \
  && apk --no-cache --no-progress add \
  bash \
  ca-certificates \
  curl \
  git \
  linux-pam \
  openssh \
  s6 \
  shadow \
  socat \
  tzdata \
  rsync
```
Установка переменной окружения.
```
ENV GOGS_CUSTOM /data/gogs
```
Копирование файла. Он находится не в каталоге `conf`, так как менять его не нужно, но он необходим для работы контейнера.
```
COPY ./data/nsswitch.conf /etc/nsswitch.conf
```
Установка рабочей директории.
```
WORKDIR /app/gogs
```
Копирование нужных для работы контейнера файлов.
```
COPY ./data/ ./docker
```
Копирование скомпилированного приложения из первого шага.
```
COPY --from=binarybuilder /gogs.io/gogs/gogs .
```
Запуск скрипта-чистильщика, который "финализирует" контейнер.
```
RUN ./docker/finalize.sh
```
Задание хранилищ. Первое - данные `gogs` приложения, второе - конфигурационные файлы `gogs`.
```
VOLUME ["/data", "/data/gogs/conf"]
```
Открытие порта.
```
EXPOSE 3000
```
Задание точки входа.
```
ENTRYPOINT ["/app/gogs/docker/start.sh"]
```
Задание перевонaчальной команды.
```
CMD ["/bin/s6-svscan", "/app/gogs/docker/s6/"]
```
